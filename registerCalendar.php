<?php
require 'calDatabase.php';


//create username and escape input
if(!empty($_POST['username'])){
	$user = $mysqli->real_escape_string($_POST['username']);
	}else{
		echo 'Error: Make sure to fill in a username.';
}
//create password and escape input
if(!empty($_POST['newPassword'])){
	$pass = $mysqli->real_escape_string($_POST['newPassword']);
	$passEncrypted = crypt($pass,'$1$WQvMDFgI$5.mVOS7V2Q/aB78Mxl13Q1');
   }else{
		echo 'Error: Make sure to fill in a password.';
}
//creat first name and escape input
if(!empty($_POST['firstName'])){
	$first = $mysqli->real_escape_string($_POST['firstName']);
   }else{
		echo 'Error: Make sure to fill in a first name.';
}
//create last name and escape input
if(!empty($_POST['lastName'])){
	$last = $mysqli->real_escape_string($_POST['lastName']);
   }else{
		echo 'Error: Make sure to fill in a last name.';
}
//create email and escape input
if(!empty($_POST['email'])){
	$email = $mysqli->real_escape_string($_POST['email']);;
   }else{
		echo 'Error: Make sure to fill in an email.';
}
//prepare and insert user into database
$stmt = $mysqli->prepare("insert into users (username, password, first_name, last_name, email) values (?,?,?,?,?)");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}else{
//bind user
	$bind = $stmt->bind_param('sssss', $user, $passEncrypted, $first, $last, $email);
	$execute = $stmt->execute();
	if(!$bind){
		printf("Bind Failed: %s\n", $mysqli->error);
		exit;
	}elseif(!$execute){
		printf("Execute Failed: %s\n", $mysqli->error);
		exit;
	}else{
		echo 'You have successfully registered. Please log in using the log in button at the top right.';
		$stmt->close();
	}    
}


?>