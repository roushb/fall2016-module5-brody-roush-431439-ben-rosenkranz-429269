<?php
require 'calDatabase.php';
//Start Login Process 
// Use a prepared statement

$stmt = $mysqli->prepare("SELECT COUNT(*), userid, username, password FROM users WHERE username=?");

// Bind the parameter
$user = $mysqli->real_escape_string($_POST['username']);
$stmt->bind_param('s', $user);
$stmt->execute();

// Bind the results
$stmt->bind_result($cnt, $user_id, $username, $pass_encrypted);
$stmt->fetch();

$pass_guess = $mysqli->real_escape_string($_POST['password']);
// Compare the submitted password to the actual password hash
if($cnt == 1 && crypt($pass_guess,$pass_encrypted)==$pass_encrypted){
		// Login succeeded!
		ini_set("session.cookie_httponly", 1);
		session_start();
		$_SESSION['user_id'] = $user_id;
		//Use an md5 hash to create a unique token to this session for further submissions.
		$_SESSION['token'] = substr(md5(rand()), 0, 10);
		$_SESSION['username'] = $username;
		// Send username to display
		echo 'Success#'.$username.'#'.$_SESSION['token'];
}else{
		// Login failed; redirect back to the login screen
		echo 'Login Failed: Please try entering your username and password again.';
}

?>