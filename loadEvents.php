<?php
require 'calDatabase.php';
ini_set("session.cookie_httponly", 1);
session_start();
// Check if user is logged in
if(isset($_SESSION['user_id'])){
    //Get userid and username
    $userID = $_SESSION['user_id'];
    $username = $_SESSION['username'];
    $stmt = $mysqli->prepare("SELECT eventid, event_Date,event_Time,event_Name,event_Desc, event_Cat FROM events WHERE event_OwnerID = ?");
	$stmt->bind_param('i',$userID);
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	 
	$stmt->execute();
	$stmt->bind_result($eventid, $date, $time, $name, $desc, $cat);
	 
	    while($stmt->fetch()){
			//Need to parse out the sql formatting to one that is taken by the addEvent form and is more readable by the user
			$date2 = date_parse_from_format("Y-m-d", $date);
			$month = $date2["month"];
			$day = $date2["day"]; 
			$safeDateEdit = strtotime($date);
			$dateNewFormat = date('n/j/Y',$safeDateEdit);
			$time_in_12_hour_format = date("g:i a", strtotime($time)); 
            echo htmlspecialchars($month).'#'.htmlspecialchars($day).'#'.htmlspecialchars($dateNewFormat).'#'.htmlspecialchars($name).'#'.htmlspecialchars($time_in_12_hour_format).'#'.htmlspecialchars($desc).'#'.htmlspecialchars($eventid).'#notshared#'.htmlspecialchars($cat).'$';
    	}
	//Check if there are any shared events for this user
	$stmt2 = $mysqli->prepare("SELECT eventid FROM sharedevents WHERE userid = ?");
	$stmt2->bind_param('i',$userID);
	if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	 
	$stmt2->execute();
	$stmt2->bind_result($eventid);
	$selectEventID = array();
	while($stmt2->fetch()){
		//Push the shared event ids onto the array stack
		array_push($selectEventID, $eventid);
		echo 'Shared event: '.$eventid;
	}
	$stmt2->close();
	foreach($selectEventID as $sharedEventID){
		$stmt3 = $mysqli->prepare("SELECT eventid, event_Date,event_Time,event_Name,event_Desc,event_Cat FROM events WHERE eventid = ?");
			
		if(!$stmt3){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt3->bind_param('i',$sharedEventID);
		$stmt3->execute();
		$stmt3->bind_result($sharedEvent, $date, $time, $name, $desc, $cat);
		while($stmt3->fetch()){
			//Same thing - have to work some magic to convert out the sql format into one readable by the user
			$date2 = date_parse_from_format("Y-m-d", $date);
			$month = $date2["month"];
			$day = $date2["day"]; 
			$safeDateEdit = strtotime($date);
			$dateNewFormat = date('n/j/Y',$safeDateEdit);
			//Convert the 24-hour time to 12-hour time
			$time_in_12_hour_format = date("g:i a", strtotime($time)); 
			echo htmlspecialchars($month).'#'.htmlspecialchars($day).'#'.htmlspecialchars($dateNewFormat).'#'.htmlspecialchars($name).'#'.htmlspecialchars($time_in_12_hour_format).'#'.htmlspecialchars($desc).'#'.htmlspecialchars($sharedEvent).'#shared#'.htmlspecialchars($cat).'$';
		}
		$stmt3->close();
	}
}
 ?>