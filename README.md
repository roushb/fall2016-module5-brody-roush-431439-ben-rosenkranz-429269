# Creative Portion - Module 5
1. Users can tag an event with a particular category and enable/disable those tags in the calendar view. To work: Login with username roushb and passoword test. Use the checkboxes at the bottom of the page to show and hide certain categories. 
2. Users can share their calendar with additional users. Login with username roushb and password test. Click on any of the calendar cells. On the right, under "share entire calendar", select the user test3. Sign out and sign in as test3 with password test. You will see all of roushb's events in red. 
3. Users can create group events that display on multiple users calendars.  Login with username roushb and password test. Click on any of the calendar cells and add an event. Check the "share with other users" box and select the user test. Sign out and sign in as test with password test. The shared event will show up in red.

Home page link: http://ec2-54-191-90-165.us-west-2.compute.amazonaws.com/~roushb/Module5/indexCalendar.php

Names: Brody Roush (431439) and Ben Rosenkranz (429269)

