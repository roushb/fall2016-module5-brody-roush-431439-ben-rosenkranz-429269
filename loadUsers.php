<?php
require 'calDatabase.php';
ini_set("session.cookie_httponly", 1);
session_start();
// Check if user is logged in
if(isset($_SESSION['user_id'])){
	if($_POST['token'] == $_SESSION['token']){
		$currentUserID = $_SESSION['user_id'];
		$stmt = $mysqli->prepare("SELECT userid,username FROM users WHERE userid != ?");
		$stmt->bind_param('i',$currentUserID);
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		 
		$stmt->execute();
		$stmt->bind_result($userid,$username);
		 
			while($stmt->fetch()){
				printf("<option value= \"%d\">%s</option>\n",
				htmlspecialchars($userid),
				htmlspecialchars($username));
		}
	}else{
		die('CSRF detected. Exiting the application');	
	}
}
 ?>