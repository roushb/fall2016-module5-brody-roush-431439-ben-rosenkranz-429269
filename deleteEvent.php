<?php
require 'calDatabase.php';
ini_set("session.cookie_httponly", 1);
session_start();
// Check if user is logged in
if(isset($_SESSION['user_id'])){
	if($_POST['token'] == $_SESSION['token']){
		//Get username
		$userID = $_SESSION['user_id'];
		$username = $_SESSION['username'];
		$eventID = $mysqli->real_escape_string($_POST['eventID']);
		//Have to delete the shared event first, if it exists due to the foreign key constraint
		$stmt = $mysqli->prepare("DELETE FROM sharedevents WHERE eventid = ?");
		$stmt->bind_param("i",$eventID);
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		 
		$stmt->execute();
		
		//Then delete from the events table
		$stmt = $mysqli->prepare("DELETE FROM events WHERE event_OwnerID = ? AND eventid = ?");
		$stmt->bind_param("ii",$userID,$eventID);
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		 
		$stmt->execute();
		echo 'Event Deleted';
		
	}else{
		die('CSRF detected. Killing application.');	
	}
}
 ?>