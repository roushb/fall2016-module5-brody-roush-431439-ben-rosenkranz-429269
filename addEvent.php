<?php
require 'calDatabase.php';
ini_set("session.cookie_httponly", 1);
session_start();
// Check if user is logged in
if(isset($_SESSION['user_id'])){
	//Get username
	$share = $mysqli->real_escape_string($_POST['eventShare']);
    $userID = $_SESSION['user_id'];
    $username = $_SESSION['username'];

    //creating an event

	//Get event name and prevent attacks
	if(!empty($_POST['eventName'])){
		$name = $_POST['eventName'];
		//Need to escape the query to prevent any attacks
		$safeName = $mysqli->real_escape_string($name);
		
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}
	   }else{
			echo 'Error: Make sure to include the name of your event.';
		}
		
   //Get event description      
   if(isset($_POST['description'])){
	if(!empty($_POST['description'])){
		$description = $_POST['description'];
		//Need to escape the query to prevent any attacks
		$safeDescription = $mysqli->real_escape_string($_POST['description']);
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}
	   }else{
			echo 'Error: Make sure to include a description of your event.';
		}

	//create time and escape input
	if(!empty($_POST['time'])){
		$eventTime = $_POST['time'];
		//Need to escape the query to prevent any attacks
		$safeTime = $mysqli->real_escape_string($_POST['time']);
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}
	   }else{
			echo 'Error: Make sure to include the time of the event.';
		}
	//create time and escape input
	if(!empty($_POST['date'])){
		$eventDate = $_POST['date'];
		//Need to escape the query to prevent any attacks
		$safeDate = $mysqli->real_escape_string($_POST['date']);
		$safeDateEdit = strtotime($safeDate);
		$dateNewFormat = date('Y-m-d',$safeDateEdit);
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}
	   }else{
			echo 'Error: Make sure to include the date of the event.';
		}
	if(!empty($_POST['eventCat'])){
		$eventCat = $_POST['eventCat'];
		//Need to escape the query to prevent any attacks
		$safeCat = $mysqli->real_escape_string($_POST['eventCat']);
		// Test for validity of token
		if($_SESSION['token'] !== $_POST['token']){
			die("ERROR: Request forgery detected. Go away please");
		}
	   }else{
			echo 'Error: Make sure to include the category of the event.';
	}
		
	//prepare and insert event into database
		$stmt = $mysqli->prepare("INSERT INTO events (event_OwnerID, event_Date, event_Time, event_Name, event_Desc, event_cat) values (?,?,?,?,?,?)");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		//bind user
		$stmt->bind_param('isssss', $userID, $dateNewFormat, $safeTime, $safeName, $safeDescription, $safeCat);
		$stmt->execute(); 
		echo 'Event Added!';
		$stmt->close(); 
   }
   if($share == 'yes'){
		$stmt2 = $mysqli->prepare("SELECT eventid FROM events WHERE event_OwnerID = ? AND event_Date = ? AND event_Time = ? AND event_Name = ? AND event_Desc = ? AND event_Cat = ?");
		if(!$stmt2){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		//bind user
		$stmt2->bind_param('isssss', $userID, $dateNewFormat, $safeTime, $safeName, $safeDescription, $safeCat);
		if(!$stmt2){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		 
		$stmt2->execute();
		$stmt2->bind_result($eventID);
		while($stmt2->fetch()){
			echo 'Event ID: '.$eventID;	
		}
		
		if(!empty($_POST['shareWithIds'])){
        //Get event name and prevent attacks
            $shareWithIds = $_POST['shareWithIds'];
            
            // Test for validity of token
            if($_SESSION['token'] !== $_POST['token']){
                die("ERROR: Request forgery detected. Go away please");
            }
	    }else{
			echo 'Error: Make sure to select people to share the event with.';
		}
            
        
		foreach ($shareWithIds as $sharedWith){
		//Need to escape the query to prevent any attacks
			$safeSharedWith = $mysqli->real_escape_string($sharedWith);
			$stmt3 = $mysqli->prepare("INSERT INTO sharedevents (eventid, userid) values (?,?)");
			if(!$stmt3){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}   
		//bind user
			$stmt3->bind_param('ii', $eventID, $safeSharedWith);
			$stmt3->execute();
			echo 'Event Shared!'; 
			$stmt3->close(); 
		
         } 
   }
   
}
 ?>