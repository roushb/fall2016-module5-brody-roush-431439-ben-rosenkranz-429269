<?php
require 'calDatabase.php';
ini_set("session.cookie_httponly", 1);
session_start();
// Check if user is logged in
if(isset($_SESSION['user_id'])){
	if($_SESSION['token'] !== $_POST['token']){
       die("ERROR: Request forgery detected. Go away please");
     }
    //Get username
    $userID = $_SESSION['user_id'];
    $username = $_SESSION['username'];
    
    $stmt2 = $mysqli->prepare("SELECT eventid FROM events WHERE event_OwnerID = ?");
		if(!$stmt2){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		//bind user
		$stmt2->bind_param('i', $userID);
		if(!$stmt2){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		 
		$stmt2->execute();
		$stmt2->bind_result($eventID);
		$allUserEventIDs = array();
		while($stmt2->fetch()){
			array_push($allUserEventIDs,$eventID);	
		}
		
		if(!empty($_POST['shareCalendarIds'])){
        
            $shareWithIds = $_POST['shareCalendarIds'];
            
	    }else{
			die('Error: Make sure to select people to share the calendar with.');
		}
            
        
		foreach ($shareWithIds as $sharedWith){
			foreach($allUserEventIDs as $eventIDShare){
			//Need to escape the query to prevent any attacks
				$safeSharedWith = $mysqli->real_escape_string($sharedWith);
				$stmt3 = $mysqli->prepare("INSERT INTO sharedevents (eventid, userid) values (?,?)");
				if(!$stmt3){
					printf("Query Prep Failed: %s\n", $mysqli->error);
					exit;
				}   
			//bind user
				$stmt3->bind_param('ii', $eventIDShare, $safeSharedWith);
				$stmt3->execute(); 
				$stmt3->close();
			}
		
         } 
		 echo 'Calendar Shared!';
  }
    

 ?>