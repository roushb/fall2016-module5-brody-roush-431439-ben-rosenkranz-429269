<?php
require 'calDatabase.php';
ini_set("session.cookie_httponly", 1);
session_start();
// Check if user is logged in
if(isset($_SESSION['user_id'])){
	if($_POST['token'] == $_SESSION['token']){
		//Get userid
		$userID = $_SESSION['user_id'];
		$username = $_SESSION['username'];
		$date = $_POST['date'];
		$safeDate = $mysqli->real_escape_string($date);
		//Need to convert string to time, then make the date sql-friendly
		$safeDateEdit = strtotime($safeDate);
		$dateNewFormat = date('Y-m-d',$safeDateEdit); 
		$stmt = $mysqli->prepare("SELECT eventid, event_Time,event_Name FROM events WHERE event_OwnerID = ? AND event_Date = ?");
		$stmt->bind_param('is',$userID, $dateNewFormat);
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		 
		$stmt->execute();
		$stmt->bind_result($eventid, $time, $name);
		 
			while($stmt->fetch()){
				//Convert from 24-hour to 12-hour time
				$time_in_12_hour_format = date("g:i a", strtotime($time)); 
				printf("<option value= \"%d\">%s at %s</option>\n",
				htmlspecialchars($eventid),
				htmlspecialchars($name),
				htmlspecialchars($time));
			}
		$stmt->close();
	}else{
		die('CSRF detected. Exiting the application');	
	}
}
 ?>